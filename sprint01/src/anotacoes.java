/**
 * Threads são subdivisões de um determinado processo.
 * Quando queremos executar mais de um procedimento ao mesmo tempo ou uma linha de instrução, utilizamos Threads.
 * O objetivo é trabalhar de forma paralela, conhecida como programação concorrente.
 *
 * Por exemplo, quando o usuário executa uma função que pode demorar muito, para o sistema não ficar
 * travado naquela requisição que foi solicitada, outros procedimentos também sejam executados.
 *
 * PROCEDIMENTOS SÍNCRONOS
 *
 * O procedimento P1 é iniciado, o procedimento P2 só irá iniciar após o término do procedimento P1 e assim sucessivamente.
 *
 * PROCEDIMENTOS ASSÍNCRONOS - THREADS
 *
 * Nesse caso podemos rodar os procedimentos todos juntos, rodando de forma paralela, ou seja, o P1 é executado junto com o P2
 * e um não dependerá do outro para iniciar/encerrar.
 *
 * Como isso é executado? O processador fica alternando a execução de cada thread, a ideia é executar um pouco de cada thread
 * fazendo a troca de uma execução para outra de forma tão rápida que dá a impressão de todas estarem sendo iniciadas ao mesmo tempo.
 *
 * Quando utilizamos mais de um processador, as threads serão executadas exatamente ao mesmo tempo.
 *
 * Se temos mais threads do que processadores, a execução será alternada, mas podemos ficar tranquilos, pois o processador
 * alterna de forma tão rápida e parece que tudo está ocorrendo ao mesmo tempo.
 */