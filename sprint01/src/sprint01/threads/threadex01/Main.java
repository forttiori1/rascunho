package br.com.lucas.threads.threadex01;

import br.com.lucas.threads.threadex02.BarraDeProgresso;
import br.com.lucas.threads.threadex02.GeraRelatorio;

public class Main {
    public static void main(String[] args) {
        BarraDeProgresso barra = new BarraDeProgresso();
        Thread t1 = new Thread(barra);
        t1.start();

        GeraRelatorio relatorio = new GeraRelatorio();
        Thread t2 = new Thread(relatorio);
        t2.start();
    }
}