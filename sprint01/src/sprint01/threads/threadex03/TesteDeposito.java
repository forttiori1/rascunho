package br.com.lucas.threads.threadex03;

public class TesteDeposito {

    public static void main(String[] args) {
        Cliente cli1 = new Cliente(27, "Lucas Rodrigues", "Rua dos Bobos");
        Conta c1 = new Conta(1, 200, 300, cli1); //Saldo 500

        FazDeposito acao = new FazDeposito(c1);
        Thread t1 = new Thread(acao);
        Thread t2 = new Thread(acao);

        t1.start(); //500 + 100 = 600?
        t2.start(); //600 + 100 = 700?

        /**porque isso acontece?
         * quando a segunda thread acessou o valor, ela ainda nao havia atualizado para 600,
         * por isso o resultado gerado foi diferente do esperado, pois as threads não são sincronizadas.
         */

        System.out.println(c1);
    }
}
