package br.com.lucas.sets;

public class Aluno {

    private String nome;
    private int numeroMatricula;

    public Aluno(String nome, int numeroMatricula) {
        if(nome == null){
            throw new NullPointerException("O nome não pode ser nulo");
        }
        this.nome = nome;
        this.numeroMatricula = numeroMatricula;
    }

    public String getNome() {
        return nome;
    }

    public int getNumeroMatricula() {
        return numeroMatricula;
    }

    @Override
    public String toString() {
        return "O aluno " + this.nome + ", está matriculado com o número " + this.numeroMatricula + ".";
    }

    public boolean equals(Object obj){
        Aluno outro = (Aluno) obj;
        return this.nome.equals(outro.nome);
    }

    /**
     * forma bastante ingenua de implementar hashcode porque se tiver muito aluno começando com a letra em questão, vai dar problema na consulta
     *

    @Override
    public int hashCode() {
        return this.nome.charAt(0);
    }*/
    /**
     * implementamos dessa forma, usando o método hashCode da classe String
     * assim o espalhamento na tabela é feito de forma mais organizada e simples de ser buscada
     */
    @Override
    public int hashCode() {
        return this.nome.hashCode();
    }
}
