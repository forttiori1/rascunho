package br.com.lucas.sets;

import br.com.lucas.arraylist.Aula;

//testacurscomaluno
public class exemplo02 {

    public static void main(String[] args) {

        Curso javaColecoes = new Curso("Estudando as coleções do Java", "Alfredo Junior");

        javaColecoes.adiciona(new br.com.lucas.arraylist.Aula("Trabalhando com ArrayList", 21));
        javaColecoes.adiciona(new br.com.lucas.arraylist.Aula("Criando uma aula", 20));
        javaColecoes.adiciona(new Aula("Modelando com coleções", 29));

        Aluno a1 = new Aluno("Lucas Rodrigues", 10201);
        Aluno a2 = new Aluno("Marcos da Silva", 10202);
        Aluno a3 = new Aluno("Carlos Alves", 10203);

        javaColecoes.matricular(a1);
        javaColecoes.matricular(a2);
        javaColecoes.matricular(a3);

        /**
         * implementar o método toString dentro da classe Aluno
         */
        System.out.println("Todos os alunos matriculados nesse curso: ");
        javaColecoes.getAlunos().forEach(a -> System.out.println(a));

        /*
        não esquece de comentar que não foi impresso com a ordem de inserção
         */

        System.out.println("*******************************");
        System.out.println("*******************************");

        System.out.println("O aluno " + a1 + " está matriculado?");
        System.out.println(javaColecoes.estaMatriculado(a1));

        System.out.println("*******************************");
        System.out.println("*******************************");

        //vai dar erro, pois não é o mesmo do conjunto anterior, é um novo aluno que foi cadastrado
        Aluno lucas = new Aluno("Lucas Rodrigues", 10201);
        System.out.println("E esse Rodrigues, está matriculado?");
        System.out.println(javaColecoes.estaMatriculado(lucas));

        //vamos testar se de fato não é o mesmo
        System.out.println("O a1 é equals ao Lucas Rodrigues?");
        System.out.println(a1.equals(lucas));
        /**
         * em um teste de false e no outro true, pq?
         * o set usa tabela de espalhamento e isso dificulta o processo de busca do nome
         * portanto precisamos reescrever o método hashCode()
         */

        //obrigatoriamente o seguinte é true

        System.out.println(a1.hashCode() == lucas.hashCode());


        System.out.println("*******************************");
        System.out.println("*******************************");
        System.out.println("*******************************");
        System.out.println("*******************************");

    }
}
