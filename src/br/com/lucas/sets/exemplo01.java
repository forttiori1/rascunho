package br.com.lucas.sets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class exemplo01 {
    public static void main(String[] args) {

        //Não damos new em Set, pois é uma interface, assim criaremos o objeto de uma das suas mais famosas implementações que é a HashSet
        Collection<String> alunos = new HashSet<>();
        alunos.add("Joao da Silva");
        alunos.add("Jose Lopes");
        alunos.add("Roberto Junior");
        alunos.add("Marcos Carvalho");
        alunos.add("Pedro Abreu");
        alunos.add("Pedro Abreu");
        alunos.add("Marcio Rodrigues");

        /**
         * ao contrário das listas, nos sets não é garantido uma ordem de exibição, pois é guardado em um sacola de elementos
         * sendo assim, não temos o método de exibir o quarto elemento da sacola, pois como mencionado, não há uma ordem.
         * portanto, não temos método get na coleção dos sets
         *
         * DIFERENÇAS
         *
         * não aceita elementos repetidos
         * usar set te entrega maior velocidade e performance
         */
        for(String aluno : alunos){
            System.out.println(aluno);
        }

        System.out.println(alunos);

        System.out.println(alunos.size());

        /**
        contains ajuda a verificar se determinada pessoa está na lista
         */
        boolean joseEstaMatriculado = alunos.contains("Jose Lopes");
        System.out.println(joseEstaMatriculado);

        //método de remoção de item da sacola
        alunos.remove("Marcos Carvalho");
        System.out.println(alunos);

        //recebendo esses dados do set em um arraylist

        System.out.println("*******************************");

        List<String> alunosEmLista = new ArrayList<>(alunos);

        System.out.println(alunosEmLista);


    }
}
