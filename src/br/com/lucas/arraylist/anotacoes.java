package br.com.lucas.arraylist;

/**
 * qual a diferença de usar arraylist e list tendo em vista que arraylist implementa a interface list?
 * a diferença é que list encapsula quem estará lá dentro, ou seja, toda vez que criamos um curso, guardamos espaço
 * para adicionar as aulas desse curso.
 * exemplo:
 * private List<Aula> aulas = new ArrayList<Aula>();
 *
 * * ARRAYLIST
 *          * arraylist permite buscar elementos de uma determinada posição de forma bastante rápida e performática, porém,
 *          * há problema para adicionar e deletar elementos que estão no meio desse arraylist, pois ela terá que mover todos os elementos da lista
 *          * por exemplo: se precisarmos adicionar um elemento na posição ZERO, ela pegará todos os outros elementos e moverá uma posição
 *          * (imagina uma lista com 10 mil elementos, demanda muito processamento)
 *
 *
 */