package br.com.lucas.arraylist.exemplo01;

import java.util.ArrayList;
import java.util.Collections;

public class TestandoListas {

    public static void main(String[] args) {
        String aula1 = "Conhecendo mais de listas";
        String aula2 = "Modelando a classe Aula";
        String aula3 = "Trabalhando com Cursos e Sets";
        String aula4 = "Aumentando nosso conhecimento";

        ArrayList<String> aulas = new ArrayList<>();
        aulas.add(aula1);
        aulas.add(aula2);
        aulas.add(aula3);
        aulas.add(aula4);
        /**
         * os itens são adicionados na lista de acordo com a ordem de inserção
         */

        System.out.println(aulas);

//        aulas.remove(0);

        System.out.println(aulas);

        System.out.println("**************************");

        /**fazemos um foreach (a partir do java 5), na verdade a palavra  não é foreach, mas chamamos assim, mas podemos usar o auto-complete
         * a sintaxe abaixo podemos ler da seguinte forma: para cada string aula dentro de aulas, faça o seguinte:
         */
        for (String aula : aulas) {
            System.out.println("Aula: " + aula);

        }

        System.out.println("**************************");

        /**
         * podemos percorrer a lista através da indice, seja em arraylist ou em mais listas, pois temos o método get.
         */

        String primeiraAula = aulas.get(0);
        System.out.println("A primeira aula é " + primeiraAula);

        System.out.println("**************************");

        /**
         * com as coleções podemos verificar o tamanho da lista com o método size()
         */

        for(int i = 0; i < aulas.size(); i++){
            System.out.println("aula: " + aulas.get(i));
        }

        //imprimindo o tamanho da lista com size
        System.out.println("O número de itens cadastrados nessa lista é de " + aulas.size());

        System.out.println("**************************");

        /**
        podemos também percorrer listas através do método forEach (sim, é um método com esse nome)
        é usado lambda nesse método
         */
        /**
         * para cada referencia ao objeto que nesse caso é aula, a gente quer que ele faça (colocar dentro das chaves), quer que imprima
         * a listagem de aulas
         */
        aulas.forEach(aula -> {
            System.out.println("Percorrendo :");
            System.out.println("Aula " + aula);
        });

        System.out.println("**************************");
        System.out.println("**************************");

        //para ordenar essa lista por ordem alfábetica através do método sort da classe Collection
        System.out.println("Ordenando por ordem alfábetica");
        Collections.sort(aulas);
        System.out.println(aulas);
    }
}
