package br.com.lucas.arraylist.exemplo02;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TestaListaDeAula {

    public static void main(String[] args) {
        Aula a1 = new Aula("Revisitando as ArrayLists", 21);
        Aula a2 = new Aula("Listas de objetos", 20);
        Aula a3 = new Aula("Relacionamento de listas e objetos", 15);

        //ArrayList de objetos
        ArrayList<Aula> aulas = new ArrayList<>();
        aulas.add(a1);
        aulas.add(a2);
        aulas.add(a3);
        //se eu tentar adicionar um string no arraylist de objetos conforme exemplo abaixo apresenta erro
        //aulas.add("Equals e Hashcode");

        /**comenta o toString da classe Aula
        imprime e fala sobre o hashcode que é herdado da classe object
        depois implementa novamente o toString*/
        System.out.println(aulas);

        System.out.println("**************************");

        /**
         * Tentando ordenar a lista já vai apresentar erro, pq ele não sabe ordenar aulas (objeto)
         * Ele vai ordenar por ordem alfabetica ou tempo? Ela não sabe fazer assim
         * Vamos implementar o método CompareTo na classe Aula.
         * Para isso basta adicionar o implements Comparable<Aula> e devemos implementar o método CompareTo
         */
        Collections.sort(aulas);
        System.out.println(aulas);

        System.out.println("**************************");

        /**
         * se eu quiser ordenar por tempo de vídeo?
         * usamos o método comparing da comparator usando duas vezes o :: e passando o getTempo
         */

        Collections.sort(aulas, Comparator.comparing(Aula::getTempo));
        System.out.println(aulas);
    }
}
