package br.com.lucas.threads.exemplo02;

public class GeraRelatorio implements Runnable{

    @Override
    public void run() {
        for(int i = 0; i < 20; i++){
            System.out.println(i + " - Gerando relatório... aguarde");
        }
    }
}
