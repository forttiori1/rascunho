package br.com.lucas.threads.exemplo03;

public class Cliente {
    private int idade;
    private String nome;
    private String endereco;

    public Cliente(int idade, String nome, String endereco) {
        this.nome = nome;
        this.endereco = endereco;

        this.dizer_oi();
    }

    public void dizer_oi(){
        System.out.println(this.nome + " está dizendo oi...");
    }

    public String getNome(){
        return this.nome;
    }

    public String getEndereco(){
        return this.endereco;
    }
}

