package br.com.lucas.threads.syncronized;

//import br.com.lucas.threads.syncronized.Conta;
//import br.com.lucas.threads.syncronized.Cliente;

public class TesteDeposito {
    /**
     * O sincronismo ocorre pois durante a execução do método a thread
     * executa um lock da função para que outra thread só possa executá-la após a finalização
     * da thread anterior
     */

    //Adicionamos o throws InterruptedException pois pode ocorrer uma exceção de interrupção.
    //Essa exceção pode ocorrer, pois pode ser que o SO alocar aquele recurso para um outro processo urgente, matando o processo pendente
    public static void main(String[] args) throws InterruptedException {
        Cliente cli1 = new Cliente(27, "Lucas Rodrigues", "Rua dos Bobos");
        Conta c1 = new Conta(1, 200, 300, cli1); //saldo 500

        FazDeposito acao = new FazDeposito(c1);
        Thread t1 = new Thread(acao);
        Thread t2 = new Thread(acao);

        t1.start(); //500 + 100 = 600?
        t2.start(); //600 + 100 = 700?

        //com o join estamos avisando que a thread t1 e t2 deve se juntar a um sincronizador
        t1.join();
        t2.join();

        System.out.println(c1);
    }
}
