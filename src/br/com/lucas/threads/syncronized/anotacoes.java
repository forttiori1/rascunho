package br.com.lucas.threads.syncronized;

/**
 * sincronizar é coordenar as atividades de 2 ou mais threads
 *
 * porque? quando 2 ou mais threads precisam acessar um recurso compartilhado, ou somente 1 thread pode acessar o recurso por vez
 * no java usamos a palavra chave synchronized em métodos (assinatura) ou também um bloco de código
 */