package br.com.lucas.valorereferencia;

import sprint01.Carro;

public class Teste1 {
    public static void main(String[] args) {

        /**usar no primeiro exemplo
         //inicializando valor com 50*/
        int valor = 50;
        //dobrando o valor
        dobrarValor(valor);

        System.out.println(valor);

        Carro c = new Carro();
        c.ano = 1997;

        mudarAnoCarro(c);
        System.out.println(c.ano);
    }

    /**método para dobrar valor e não retornar nada
     //passando o valor por parâmetro, acabamos instanciando esse novo parametro para uma cópia desse parametro
     //ou seja, não é a mesma variável
     * */

    /**
     * no início do método dobrar valor ele está como 50, porém, no final do método ele passa a ter o valor 100
     * fazendo passagem por valor.
     * é possível fazer com os tipos primitivos do java tal qual o int, double, boolean
     */
    public static void dobrarValor(int v){
        v = v*2;
    }

    /**método para pegar o ano do carro e somar 1
     * nesse caso estamos passando diretamente o objeto 'Carro c'
     * nesse método eu estou acessando o ano desse objeto e somando mais 1, gerando o valor 1998
     *
     */
    public static void mudarAnoCarro(Carro c){
        c.ano = c.ano + 1;
    }
}