package br.com.lucas.armazenamentosequencial;

import java.util.Arrays;

public class Vetor {

    private Aluno alunos[] = new Aluno[100];
    private int total = 0;

    public void adiciona(Aluno aluno){
        this.alunos[total] = aluno;
        total = total + 1;
    }

    public void adiciona(int posicao, Aluno aluno){
        for(int i = total -1; i>= posicao; i-=1){
            alunos[i+1] = alunos[i];
        }
    }

    public Aluno pega(int posicao) {
        if(!posicaoOcupada(posicao)){
            throw new IllegalArgumentException("Posição inválida!");
        }
        return this.alunos[posicao];
    }

    public void remove(int posicao){
        this.alunos[posicao] = null;
    }

    //método para descobrir se o aluno está ou não na lista
    public boolean contem(Aluno aluno){
        for(int i = 0; i < total; i++){
            if(aluno.equals(alunos[i])){
                return true;
            }
        }
        return false;
    }

    //devolve a quantidade de alunos no vetor. lá em cima estamos falando do tamanho do vetor
    //e não da quantidade de alunos cadastrado
    public int tamanho(){
        return this.total;
    }

    //facilita a visualização do array
    @Override
    public String toString() {
        return Arrays.toString(this.alunos);
    }

    private boolean posicaoOcupada(int posicao){
        return posicao >= 0 && posicao < total;
    }
}
