package br.com.lucas.ArrayxLinked;

import java.util.ArrayList;
import java.util.LinkedList;

public class Main {

    public static void main(String args[]) {

        ArrayList arrayList = new ArrayList();
        LinkedList linkedList = new LinkedList();

        // ArrayList add
        long inicioDaContagem = System.nanoTime();

        for (int i = 0; i < 100000; i++) {
            arrayList.add(i);
        }
        long finalDaContagem = System.nanoTime();
        long duracao = finalDaContagem - inicioDaContagem;
        System.out.println("Adicionando com ArrayList:  " + duracao);

        // LinkedList add
        inicioDaContagem = System.nanoTime();

        for (int i = 0; i < 100000; i++) {
            linkedList.add(i);
        }
        finalDaContagem = System.nanoTime();
        duracao = finalDaContagem - inicioDaContagem;
        System.out.println("Adicionando com LinkedList: " + duracao);

        // ArrayList get
        inicioDaContagem = System.nanoTime();

        for (int i = 0; i < 10000; i++) {
            arrayList.get(i);
        }
        finalDaContagem = System.nanoTime();
        duracao = finalDaContagem - inicioDaContagem;
        System.out.println("Get com ArrayList  " + duracao);

        // LinkedList get
        inicioDaContagem = System.nanoTime();

        for (int i = 0; i < 10000; i++) {
            linkedList.get(i);
        }
        finalDaContagem = System.nanoTime();
        duracao = finalDaContagem - inicioDaContagem;
        System.out.println("Get com LinkedList: " + duracao);

        // ArrayList remove
        inicioDaContagem = System.nanoTime();

        for (int i = 9999; i >= 0; i--) {
            arrayList.remove(i);
        }
        finalDaContagem = System.nanoTime();
        duracao = finalDaContagem - inicioDaContagem;
        System.out.println("Removendo com ArrayList:  " + duracao);

        // LinkedList remove
        inicioDaContagem = System.nanoTime();

        for (int i = 9999; i >= 0; i--) {
            linkedList.remove(i);
        }
        finalDaContagem = System.nanoTime();
        duracao = finalDaContagem - inicioDaContagem;
        System.out.println("Removendo com LinkedList: " + duracao);

    }
}
