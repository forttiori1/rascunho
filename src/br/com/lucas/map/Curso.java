package br.com.lucas.map;

import java.util.*;

public class Curso {

    private String nome;
    private String instrutor;
    private List<Aula> aulas = new LinkedList<>();
    private Set<Aluno> alunos = new HashSet<>();
    private Map<Integer, Aluno> matriculaParaAluno = new HashMap<>();


    /**se em algum momento precisarmos mudar para linkedlist, teremos problemas
     * private List<Aula> aulas = new LinkedList<Aula>(); porque uma linkedlist não é filha de arraylist, ela somente implementa list.
     * por isso é melhor usar list, pois é mais genérico e ganhamos baixo acoplamento
     */

    public Curso(String nome, String instrutor) {
        this.nome = nome;
        this.instrutor = instrutor;
    }

    public List<Aula> getAulas(){
        //adicionando dessa forma os itens adicionados não poderão ser alterados/modificados
        return Collections.unmodifiableList(aulas);
    }

    public String getNome() {
        return nome;
    }

    public String getInstrutor() {
        return instrutor;
    }

    /**
     * toda vez que alguém adicionar aluno
     * além de adicionar no set de alunos, vai adicionar no mapa uma relação entre o número de matricula do aluno com ele mesmo
     */

    public void adicionar(Aula aula){
        this.aulas.add(aula);
    }

    public int getTempoTotal(){
        int tempoTotal = 0;
        for (Aula aula : aulas){
            tempoTotal += aula.getTempo();
        }
        return tempoTotal;
    }

    @Override
    public String toString() {
        return "Curso: " + nome + ", o seu tempo total é de  " + this.getTempoTotal() + " minutos.";
    }

    public void matricular(Aluno aluno) {
        this.alunos.add(aluno);
        this.matriculaParaAluno.put(aluno.getNumeroMatricula(), aluno);
    }

    public Set<Aluno> getAlunos(){
        return Collections.unmodifiableSet(alunos);
    }

    public boolean estaMatriculado(Aluno aluno){
        return this.alunos.contains(aluno);
    }
    /**
     * adicionando o número implementado no método matricular
     */
    public Aluno buscaMatriculado (int numero){
        if(!matriculaParaAluno.containsKey(numero)) {
            throw new NoSuchElementException("A matricula não foi encontrada!");
        }else{
            return matriculaParaAluno.get(numero);
        }
    }
}