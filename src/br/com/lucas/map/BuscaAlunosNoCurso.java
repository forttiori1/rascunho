package br.com.lucas.map;

/**
 * CRIAR PROBLEMA
 *
 * se precisarmos encontrar determinada matricula em um banco de dados, vamos gastar bastante tempo perguntando
 * se os conjuntos possuem determinada matricula e isso vai demandar bastante memória
 * para isso podemos usar o map, que usa associações, como se fosse uma tabela, que nesse exemplo vai armazenar a matricula atrelada ao nome
 *
 */

/**
 * LINKEDHASHMAP
 * guarda a ordem em que foi inserido os puts
 */

public class BuscaAlunosNoCurso {

    public static void main(String[] args) {

        Curso javaColecoes = new Curso("Aprendendo as coleções do Java","João da Silva");

        javaColecoes.adicionar(new Aula("Trabalhando com ArrayList", 21));
        javaColecoes.adicionar(new Aula("Trabalhando com ArrayList", 21));
        javaColecoes.adicionar(new Aula("Criando uma Aula", 20));
        javaColecoes.adicionar(new Aula("Modelando com coleções", 24));

        Aluno a1 = new Aluno("José Alves", 34672);
        Aluno a2 = new Aluno("Alfredo Junior", 5617);
        Aluno a3 = new Aluno("Carlos Fernando", 17645);
        //usar no exemplo de alunos com mesma matricula
        Aluno a4 = new Aluno("José Alfredo", 17645);

        javaColecoes.matricular(a1);
        javaColecoes.matricular(a2);
        javaColecoes.matricular(a3);
        javaColecoes.matricular(a4);

        /**
         * vamos verificar se determinado aluno está ou não matriculado
         */

        System.out.println("Quem é o aluno com matricula 5619?");
        Aluno aluno = javaColecoes.buscaMatriculado(5619);
        System.out.println("Aluno: " + aluno);
        //usar no exemplo de adicionar alunos com a mesma matricula
        /**
         * se eu pesquisar pela matricula que está repetida, ele vai devolver na resposta
         * o nome do último put que foi efetuado
         */
        System.out.println(javaColecoes.getAlunos());
    }
}
