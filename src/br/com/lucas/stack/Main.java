package br.com.lucas.stack;

import java.util.Stack;

public class Main {

    public static void main(String[] args) {

        Stack<Integer> stack = new Stack<Integer>();

        System.out.println(stack.isEmpty());

        stack.push(1);
        stack.push(2);
        stack.push(3);

        System.out.println(stack.isEmpty());
        System.out.println(stack.size());
        System.out.println(stack);

        System.out.println("Espiando o último item da pilha");
        System.out.println(stack.peek());

        //Vai imprimir o 3 e remove-lo
        System.out.println(stack.pop());

        System.out.println(stack);

    }
}
