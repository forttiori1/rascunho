package br.com.lucas.queue;

import java.util.LinkedList;
import java.util.Queue;

public class Main {

    public static void main(String[] args) {

        /**
         * não é possível instanciar a classe Queue não existe no java, o que existe é a interface Queue
         *
         */

        //Queue<Integer> fila = new Queue<Integer>();
//
//        Interface1 c1 = new Classe1();
//
//        c1.adiciona();
//        c1.remove();

        //esse método não fica disponível, pois declaramos a interface e não a classe
        //c1.teste();

        /**
         * declaramos a queue, portanto teremos que implementar somente os métodos da Queue, garantindo o comportamento FIFO (comportamento de fila na classe
         * primeiro que entra é o primeiro que saí
         */

        Queue<Integer> fila = new LinkedList<>();

        fila.add(1);
        fila.add(2);
        fila.add(3);
        fila.add(4);

        System.out.println(fila);

        //espiando
        /**
         * na fila ele remove e espia o elemento da primeira posição, ao contrário da stack
         */
        System.out.println(fila.peek());
        System.out.println(fila.remove());

        System.out.println(fila);


    }
}
