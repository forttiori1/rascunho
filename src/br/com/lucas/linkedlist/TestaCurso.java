package br.com.lucas.linkedlist;

import java.util.List;

public class TestaCurso {

    public static void main(String[] args) {
        Curso javaColecoes = new Curso("Estudando as coleções do Java", "João da Silva");

        /**
         * pegando o getAulas para devolver uma lista de aulas
         */
        //List<Aula> aulas = javaColecoes.getAulas();
        /**
         * vamos imprimir e vai dar vazio, mas porque?
         * porque a lista está vazia
         */
        //System.out.println(aulas);

        /**
         * adcionando aulas na lista
         * vamos adicionar dessa forma para que seja mais simples e em menos linhas
         */

        /**
        dentro de getAulas foi implementado Collections.unmodifiableList e assim não poderemos adicionar aulas
        através do add da coleção, somente adicionando através do método implementado na classe curso
        javaColecoes.getAulas().add(new Aula("Trabalhando com ArrayList", 21));
        usando o método adiciona para simplificar a adição no curso, não usando o método da coleção
        dessa forma para poder adicionar terá que solicitar diretamente ao curso


        sendo assim, para adicionar o curso, não teremos que abrir o capô do carro e mexer lá dentro (usar o exemplo do carro)
        provavelmente serei questionado se o meu método getAulas não pode ser alterado, vou dizer que pode ser alterado
         mas para que não passe por isso, dentro do método adiciona vou aplicar o Collections.unmodifiableList
         */

        javaColecoes.adiciona(new Aula("Trabalhando com ArrayList", 21));
        javaColecoes.adiciona(new Aula("Criando uma aula", 20));
        javaColecoes.adiciona(new Aula("Modelando com coleções", 29));

        //imprimindo apenas a aula, ou seja, ela aponta para lista, pois são a mesma coisa
        //eles se referenciam exatamente para o mesmo objeto
        System.out.println(javaColecoes.getAulas());

        /**
         * diferença entre arraylist e linkedlist?
         * as duas são implementações de list que respeitam a mesma regra, ou seja, mantem a ordem que foi adicionado
         * pode também adicionar elementos repetidos
         *
         * a principal diferença é basicamente de performance, pois listas muito longas exigem processamento.
         *
         * ARRAYLIST
         * arraylist permite buscar elementos de uma determinada posição de forma bastante rápida e performática, porém,
         * há problema para adicionar e deletar elementos que estão no meio desse arraylist, pois ela terá que mover todos os elementos da lista
         * por exemplo: se precisarmos adicionar um elemento na posição ZERO, ela pegará todos os outros elementos e moverá uma posição
         * (imagina uma lista com 10 mil elementos, demanda muito processamento)
         *
         *
         * LINKEDLIST
         *
         * a linkedlist é muito rápida para inserir e remover elementos no começo, porém, ela onera muito no processamento quando precisamos
         * de um elemento de determinada posição.
         * por exemplo: digamos que seja solicitado um elemento da posição 1000, ela percorrerá a lista do começo até chegar na posição mencionada
         * se for uma lista enorme, ela percorrerá muitos elementos, gerando alto processamento
         */

        List<Aula> aulas = javaColecoes.getAulas();
        System.out.println(aulas);

        System.out.println(javaColecoes.getTempoTotal());

        System.out.println(javaColecoes);
    }
}