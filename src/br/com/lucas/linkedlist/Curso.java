package br.com.lucas.linkedlist;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Curso {

    private String nome;
    private String instrutor;
    private List<Aula> aulas = new LinkedList<>();

    /**se em algum momento precisarmos mudar para linkedlist, teremos problemas
     * private List<Aula> aulas = new LinkedList<Aula>(); porque uma linkedlist não é filha de arraylist, ela somente implementa list.
     * por isso é melhor usar list, pois é mais genérico e ganhamos baixo acoplamento
     */

    public Curso(String nome, String instrutor) {
        this.nome = nome;
        this.instrutor = instrutor;
    }

    public List<Aula> getAulas(){
        //adicionando dessa forma os itens adicionados não poderão ser alterados/modificados
        return Collections.unmodifiableList(aulas);
    }

    public String getNome() {
        return nome;
    }

    public String getInstrutor() {
        return instrutor;
    }

    public void adiciona(Aula aula){
        this.aulas.add(aula);
    }

    public int getTempoTotal(){
        int tempoTotal = 0;
        for (Aula aula : aulas){
            tempoTotal += aula.getTempo();
        }
        return tempoTotal;
    }

    @Override
    public String toString() {
        return "Curso: " + nome + ", o seu tempo total é de  " + this.getTempoTotal() + " minutos.";
    }
}

